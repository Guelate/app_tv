package application;

import java.net.URI;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;

import javax.swing.*;
import javax.swing.JFrame;
import javax.swing.JButton;
//import java.net.URI;
import java.net.URISyntaxException;



public class Vue implements ActionListener {
	
	public String lien[] = {"programme-tf1-19.html","programme-france-2-6.html","programme-france-3-7.html","programme-canalplus-2.html","programme-france-5-9.html","programme-m6-12.html","programme-arte-337.html","programme-c8-4.html","programme-w9-24.html","programme-tmc-21.html"};
	public String chaine[] = {"Tf1","France 2","France 3","Canal +","France 5","M6","Arte","C8","W9","TMC"};
	public int hauteur = 0;
	
	JFrame fenêtre;
	JButton button;
	
	public Vue(String nom){
		
		fenêtre = new JFrame(nom);
		fenêtre.setSize(new Dimension(800,1300));
		Color c = new Color(66,80,82);
		fenêtre.getContentPane().setBackground(c);
		fenêtre.setLayout(null);
		fenêtre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fenêtre.setLocationRelativeTo(null);
		
		fenêtre.setVisible(true);
		
		for (int i = 0; i < chaine.length; i++) {
			button = new JButton(chaine[i]);
			button.setBounds(280,hauteur = hauteur+110,220,80);
			button.setFont(new Font("Comic Sans",Font.ITALIC,30));
			button.setBackground(new Color(159,202,205));
			button.addActionListener(this);
			fenêtre.add(button);
		}
	}
	
	
	public void actionPerformed(ActionEvent evt) {

		String source = evt.getActionCommand();
		switch(source) {
		  case "Tf1":
			URI uri1 = URI.create("https://www.programme-tv.net/programme/chaine/" + lien[0]);
			try {
				Desktop.getDesktop().browse(uri1);
			} catch (IOException e) {
				//
				e.printStackTrace();
			}
		    break;
		  case "France 2":
			  URI uri2 = URI.create("https://www.programme-tv.net/programme/chaine/" + lien[1]);
			  try {
				  Desktop.getDesktop().browse(uri2);
			  } catch (IOException e) {
				//
				  e.printStackTrace();
			  }
			  break;
		  case "France 3":
				URI uri3 = URI.create("https://www.programme-tv.net/programme/chaine/" + lien[2]);
				try {
					Desktop.getDesktop().browse(uri3);
				} catch (IOException e) {
					//
					e.printStackTrace();
				}
			    break;
		  case "Canal +":
			  URI uri4 = URI.create("https://www.programme-tv.net/programme/chaine/" + lien[3]);
			  try {
				  Desktop.getDesktop().browse(uri4);
			  } catch (IOException e) {
				//
				  e.printStackTrace();
			  }
			  break;
		  case "France 5":
				URI uri5 = URI.create("https://www.programme-tv.net/programme/chaine/" + lien[4]);
				try {
					Desktop.getDesktop().browse(uri5);
				} catch (IOException e) {
					//
					e.printStackTrace();
				}
			    break;
		  case "M6":
			  URI uri6 = URI.create("https://www.programme-tv.net/programme/chaine/" + lien[5]);
			  try {
				  Desktop.getDesktop().browse(uri6);
			  } catch (IOException e) {
				//
				  e.printStackTrace();
			  }
			  break;
		  case "Arte":
				URI uri7 = URI.create("https://www.programme-tv.net/programme/chaine/" + lien[6]);
				try {
					Desktop.getDesktop().browse(uri7);
				} catch (IOException e) {
					//
					e.printStackTrace();
				}
			    break;
		  case "D8":
			  URI uri8 = URI.create("https://www.programme-tv.net/programme/chaine/" + lien[7]);
			  try {
				  Desktop.getDesktop().browse(uri8);
			  } catch (IOException e) {
				//
				  e.printStackTrace();
			  }
			  break;
		  case "W9":
				URI uri9 = URI.create("https://www.programme-tv.net/programme/chaine/" + lien[8]);
				try {
					Desktop.getDesktop().browse(uri9);
				} catch (IOException e) {
					//
					e.printStackTrace();
				}
			    break;
		  case "TMC":
			  URI uri10 = URI.create("https://www.programme-tv.net/programme/chaine/" + lien[9]);
			  try {
				  Desktop.getDesktop().browse(uri10);
			  } catch (IOException e) {
				
				  e.printStackTrace();
			  }
			  break;
		  default:
		    
		}
	}
}		
		


